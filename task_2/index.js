const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8080;
const password = "abrakadabra";

app.get('/', (req, res) => {
    res.send("Hello, now you are at homepage");
})

app.get('/encode/:word', ((req, res) => {
    res.send(Vigenere.Cipher(password).crypt(req.params.word));
}));

app.get('/decode/:word', ((req, res) => {
    res.send(Vigenere.Decipher(password).crypt(req.params.word));
}));

app.listen(port, ()=> {
    console.log("Port: " + port);
})