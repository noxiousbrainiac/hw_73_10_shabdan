const express = require('express');

const app = express();
const port = 8000;

app.get('/', ((req, res) => {
    res.send("Hello Abrakadabra");
}));

app.get('/:name', (req, res)=> {
    res.send('Hello, ' + req.params.name);
});

app.listen(port, () => {
    console.log('Port: ' + port);
});
